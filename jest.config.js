module.exports = {
  testMatch: ["**/*.(test|spec).+(js|jsx|ts|tsx)"],
  modulePathIgnorePatterns: ["<rootDir>/node_modules"],
  collectCoverage: true,
  collectCoverageFrom: [
    "src/**/*.js",
    "!**node_modules/**",
    "!src/server.js",
    "!src/errors/**",
    "!src/models/**",
    "!src/config/**",
  ],
  coverageDirectory: "coverage",
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
};
