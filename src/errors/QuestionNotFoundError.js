class QuestionNotFoundError extends Error {
  constructor(...params) {
    super(`Failed to find a question for: ${params}`, ...params);

    // Maintains proper stack trace for where our error is thrown
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, QuestionNotFoundError);
    }

    this.name = "QuestionNotFoundError";
    this.status = 404;
  }
}

module.exports = QuestionNotFoundError;
