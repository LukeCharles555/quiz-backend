const jwt_decode = require("jwt-decode");

const isUserAuthorized = (request) => {
  const token = request.headers.authorization;
  const user = jwt_decode(token);

  if (user.roles.includes("Admin")) {
    return true;
  }
  return false;
};

module.exports = isUserAuthorized;
