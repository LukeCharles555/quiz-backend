const bodyParser = require("body-parser");
const passport = require("passport");
const cors = require("cors");

const questionsRouter = require("../routes/questions/questionsRouter");
const userRouter = require("../routes/users/userRouter");
const passportConfig = require("../config/passport");

const initialiseServer = (app) => {
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  app.use(passport.initialize());
  app.use(cors());

  passportConfig(passport);

  app.use("/users", userRouter(app));
  app.use("/questions", questionsRouter(app));

  return app;
};

module.exports = initialiseServer;
