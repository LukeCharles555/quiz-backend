const questionsRouter = require("../routes/questions/questionsRouter");
const initialiseServer = require("./initialiseServer");

/**
 * Here we can mock out our modules so we aren't using them directly.
 * This is so we can just test initialiseServer in isolation.
 */
jest.mock("body-parser", () => ({
  urlencoded: () => {},
  json: () => {},
}));

jest.mock("../routes/questions/questionsRouter", () => jest.fn());
jest.mock("../config/passport", () => jest.fn());
jest.mock("passport", () => ({
  initialize: () => {},
}));
jest.mock("cors", () => jest.fn());

const app = {
  use: (callbackOne, callbackTwo) => {
    if (typeof callbackOne === "function") {
      callbackOne();
    }
  },
};

describe("initialiseServer", () => {
  test("should call to set up endpoints", () => {
    initialiseServer(app);

    expect(questionsRouter).toHaveBeenCalled();
  });
});
