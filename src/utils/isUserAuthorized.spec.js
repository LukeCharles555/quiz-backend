const isUserAuthorized = require("./isUserAuthorized");
const jwt_decode = require("jwt-decode");

jest.mock("jwt-decode", () => jest.fn());

const mockRequest = {
  headers: {
    authorization: "some-token",
  },
};

describe("isUserAuthorized", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should return true when isUserAuthorized is called with an admin account", () => {
    jwt_decode.mockReturnValue({
      roles: ["Admin"],
    });
    expect(isUserAuthorized(mockRequest)).toEqual(true);
  });

  test("should return false when isUserAuthorized is called without an admin account", () => {
    jwt_decode.mockReturnValue({
      roles: ["User"],
    });
    expect(isUserAuthorized(mockRequest)).toEqual(false);
  });
});
