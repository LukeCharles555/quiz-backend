const QuestionNotFoundError = require("../../errors/QuestionNotFoundError");
const Question = require("../../models/Question");
const {
  createQuestion,
  getQuestions,
  updateQuestion,
  deleteQuestion,
} = require("./questionRequests");

const mockRequest = {
  params: { id: "test-id" },
  body: {
    description: "Test",
    answers: [{}],
  },
};

jest.mock("../../models/Question.js", () => ({
  create: jest.fn(),
  find: jest.fn(),
  findOne: jest.fn(),
  save: jest.fn(),
  deleteOne: jest.fn(),
}));

jest.mock("../../utils/isUserAuthorized", () =>
  jest.fn().mockReturnValue(true)
);

describe("questionsRequests", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should call Question.create when createQuestion has a request body sent to it", () => {
    createQuestion(mockRequest);
    expect(Question.create).toHaveBeenCalledWith(mockRequest.body);
  });

  test("should call Question.find when getQuestions is called", () => {
    getQuestions();
    expect(Question.find).toHaveBeenCalled();
  });

  test("should call Question.findOne when updateQuestion is called", () => {
    updateQuestion(mockRequest);
    expect(Question.findOne).toHaveBeenCalledWith({
      _id: mockRequest.params.id,
    });
  });

  test("should return a custom error when updateQuestion is called and it can't find a question", () => {
    Question.findOne.mockResolvedValue(false);
    return updateQuestion(mockRequest)
      .then(() => {
        fail();
      })
      .catch((error) => {
        expect(error).toBeInstanceOf(QuestionNotFoundError);
      });
  });

  test("should call Question.deleteOne when deleteQuestion is called and return an error if it can't find a question", () => {
    Question.deleteOne.mockResolvedValue({ deletedCount: 0 }); // Forcing deleteOne to not find a question so i can test my custom error
    return deleteQuestion(mockRequest)
      .then(() => {
        expect(Question.deleteOne).toHaveBeenCalledWith({
          _id: mockRequest.params.id,
        });
      })
      .catch((error) => {
        expect(error).toBeInstanceOf(QuestionNotFoundError);
      });
  });

  test("should return the deleted question when deleteQuestion is called", () => {
    Question.deleteOne.mockResolvedValue(mockRequest.body);
    return deleteQuestion(mockRequest)
      .then((response) => {
        expect(response).toBe(mockRequest.body);
      })
      .catch(() => {
        fail();
      });
  });
});
