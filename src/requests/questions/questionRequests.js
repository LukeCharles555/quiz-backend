const QuestionNotFoundError = require("../../errors/QuestionNotFoundError");
const UserNotAuthorizedError = require("../../errors/UserNotAuthorizedError");
const Question = require("../../models/Question");
const isUserAuthorized = require("../../utils/isUserAuthorized");

const createQuestion = (request) => {
  const { description } = request.body;
  const { answers } = request.body;

  if (isUserAuthorized(request)) {
    const question = Question.create({
      description,
      answers,
    });

    return question;
  } else {
    return Promise.reject(new UserNotAuthorizedError());
  }
};

/**
 * Dont need to check if user is authorized here, users with no permission will use this path
 * to take the quiz.
 * Only need auth checks if the user is trying to create/update or delete a question
 */
const getQuestions = (request) => {
  const questions = Question.find();

  return questions;
};

const getQuestionById = (request) => {
  const id = request.params.id;
  const questions = Question.findById(id);

  return questions;
};

const updateQuestion = async (request) => {
  const _id = request.params.id;
  const { description, answers } = request.body;

  if (isUserAuthorized(request)) {
    let questionToUpdate = await Question.findOne({ _id });

    if (!questionToUpdate) {
      return Promise.reject(new QuestionNotFoundError(_id));
    } else {
      questionToUpdate.description = description;
      questionToUpdate.answers = answers;
      await questionToUpdate.save();
      return questionToUpdate;
    }
  } else {
    return Promise.reject(new UserNotAuthorizedError());
  }
};

const deleteQuestion = async (request) => {
  const _id = request.params.id;

  if (isUserAuthorized(request)) {
    const question = await Question.deleteOne({ _id });

    // Here we can tell the user a helpful error message if a question isnt found in the DB
    if (question.deletedCount === 0) {
      return Promise.reject(new QuestionNotFoundError(_id));
    } else {
      return question;
    }
  } else {
    return Promise.reject(new UserNotAuthorizedError());
  }
};

module.exports = {
  createQuestion,
  getQuestions,
  updateQuestion,
  deleteQuestion,
  getQuestionById,
};
