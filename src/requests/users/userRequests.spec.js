const ValidationError = require("../../errors/ValidationError");
const EmailNotFoundError = require("../../errors/EmailNotFoundError");
const PasswordIncorrectError = require("../../errors/PasswordIncorrectError");

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const User = require("../../models/User");
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
const { registerUser, loginUser } = require("./userRequests");

const mockRegisterRequest = {
  body: {
    name: "TestName",
    email: "test@test.com",
    password: "testPassword",
  },
};

const mockLoginRequest = {
  body: {
    email: "test@test.com",
    password: "testPassword",
  },
};

jest.mock("../../models/User.js", () => ({
  findOne: jest.fn(),
  save: jest.fn(),
}));

jest.mock("bcryptjs", () => ({
  compare: jest.fn(),
}));

// Mocking these so i can change if the request is valid or not
jest.mock("../../validation/register.js", () => jest.fn());
jest.mock("../../validation/login.js", () => jest.fn());

describe("userRequests", () => {
  describe("registerUser", () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    test("should return a custom ValidationError if the register input isnt valid", () => {
      validateRegisterInput.mockReturnValue({
        errors: "testError",
        isValid: false,
      });
      return registerUser(mockRegisterRequest)
        .then(() => {
          fail();
        })
        .catch((error) => {
          expect(error).toBeInstanceOf(ValidationError);
        });
    });

    test("should throw a validation error when a user is already registered", () => {
      validateRegisterInput.mockReturnValue({ isValid: true });
      User.findOne.mockResolvedValue(true);
      return registerUser(mockRegisterRequest)
        .then(() => {
          fail();
        })
        .catch((error) => {
          expect(error).toBeInstanceOf(ValidationError);
        });
    });
  });

  describe("loginUser", () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    test("should return a custom ValidationError if the register input isnt valid", () => {
      validateLoginInput.mockReturnValue({
        errors: "testError",
        isValid: false,
      });

      return loginUser(mockLoginRequest)
        .then(() => {
          fail();
        })
        .catch((error) => {
          expect(error).toBeInstanceOf(ValidationError);
        });
    });

    test("should throw a validation error when an email is not found", () => {
      validateLoginInput.mockReturnValue({ isValid: true });
      User.findOne.mockResolvedValue(false);

      return loginUser(mockLoginRequest)
        .then(() => {
          fail();
        })
        .catch((error) => {
          expect(error).toBeInstanceOf(EmailNotFoundError);
        });
    });

    test("should throw a validation error when the password is incorrect", () => {
      validateLoginInput.mockReturnValue({ isValid: true });
      User.findOne.mockResolvedValue({
        id: "test-id",
        name: "TestName",
        email: "test@test.com",
        password: "should-fail",
      });
      bcrypt.compare.mockResolvedValue(false);

      return loginUser(mockLoginRequest)
        .then(() => {
          fail();
        })
        .catch((error) => {
          expect(error).toBeInstanceOf(PasswordIncorrectError);
        });
    });
  });
});
