const express = require("express");
const router = express.Router();

const {
  createQuestion,
  getQuestions,
  updateQuestion,
  deleteQuestion,
  getQuestionById,
} = require("../../requests/questions/questionRequests");

const questionsRouter = (app) => {
  router.get("/questionHealth", (request, response) => {
    response.send("This is an API health check");
  });
  router.get("/getQuestions", (request, response, next) => {
    return getQuestions(request)
      .then((questions) => {
        app.logger.info("Questions retrieved");
        app.logger.trace(questions);
        response.jsonp(questions);
      })
      .catch((error) => {
        app.logger.error(error);
        next(error);
      });
  });

  router.get("/getQuestions/:id", (request, response, next) => {
    return getQuestionById(request)
      .then((question) => {
        app.logger.info("Question retrieved");
        app.logger.trace(question);
        response.jsonp(question);
      })
      .catch((error) => {
        app.logger.error(error);
        next(error);
      });
  });

  router.post("/createQuestion", (request, response, next) => {
    return createQuestion(request)
      .then(({ data: question }) => {
        app.logger.info("Question created");
        app.logger.trace(question);
        response.jsonp(question);
      })
      .catch((error) => {
        app.logger.error(error);
        next(error);
      });
  });

  router.put("/updateQuestion/:id", (request, response, next) => {
    return updateQuestion(request)
      .then(({ data: question }) => {
        app.logger.info("Question updated successfully");
        app.logger.trace(question);
        response.jsonp(question);
      })
      .catch((error) => {
        app.logger.error(error);
        next(error);
      });
  });

  router.delete("/deleteQuestion/:id", (request, response, next) => {
    return deleteQuestion(request)
      .then((question) => {
        app.logger.info("Question deleted successfully");
        app.logger.trace(question);
        response.jsonp(question);
      })
      .catch((error) => {
        app.logger.error(error);
        next(error);
      });
  });

  return router;
};

module.exports = questionsRouter;
