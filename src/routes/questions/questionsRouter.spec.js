const questionsRouter = require("./questionsRouter");
const questionsRequests = require("../../requests/questions/questionRequests");

const mockGet = jest.fn();
const mockPost = jest.fn();
const mockPut = jest.fn();
const mockDelete = jest.fn();

const mockQuestion = {};

const mockRequest = {
  body: mockQuestion,
};

const mockResponse = {
  jsonp: jest.fn(),
  status: jest.fn(),
  send: jest.fn(),
};

const mockNext = jest.fn();

/**
 * We dont care what the output of these are for unit tests, so we can set them as jest mock functions.
 * We can test if they're called at the correct times.
 */
const app = {
  logger: {
    error: jest.fn(),
    info: jest.fn(),
    trace: jest.fn(),
  },
};

/**
 * Mocking express because we are testing the code, not express
 */
jest.mock("express", () => ({
  Router: () => ({
    get: (string, callback) => {
      mockGet(string);
      callback(mockRequest, mockResponse, mockNext);
    },
    post: (string, callback) => {
      mockPost(string);
      callback(mockRequest, mockResponse, mockNext);
    },
    put: (string, callback) => {
      mockPut(string);
      callback(mockRequest, mockResponse, mockNext);
    },
    delete: (string, callback) => {
      mockDelete(string);
      callback(mockRequest, mockResponse, mockNext);
    },
  }),
}));

/**
 * Mocking these because I have tested these in isolation in questionRequests.spec.js
 * Can mock what I want back from these functions so the test knows how to act
 */
jest.mock("../../requests/questions/questionRequests.js", () => ({
  createQuestion: jest
    .fn()
    .mockImplementation(() => Promise.resolve({ data: mockQuestion })),
  getQuestions: jest
    .fn()
    .mockImplementation(() => Promise.resolve([mockQuestion])),
  getQuestionById: jest
    .fn()
    .mockImplementation(() => Promise.resolve(mockQuestion)),
  updateQuestion: jest
    .fn()
    .mockImplementation(() => Promise.resolve({ data: mockQuestion })),
  deleteQuestion: jest
    .fn()
    .mockImplementation(() => Promise.resolve(mockQuestion)),
}));

const flushPromises = () => new Promise(setImmediate);

describe("questionsRouter", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe("questionsRouter initial endpoints", () => {
    test("should create a /questionHealth endpoint", () => {
      questionsRouter(app);
      expect(mockGet).toHaveBeenCalledWith("/questionHealth");
      expect(mockResponse.send).toHaveBeenCalledWith(
        "This is an API health check"
      );
    });

    test("should create a /getQuestions endpoint", () => {
      questionsRouter(app);
      expect(mockGet).toHaveBeenCalledWith("/getQuestions");
    });

    test("should create a /getQuestions/:id endpoint", () => {
      questionsRouter(app);
      expect(mockGet).toHaveBeenCalledWith("/getQuestions/:id");
    });

    test("should create a /createQuestion endpoint", () => {
      questionsRouter(app);
      expect(mockPost).toHaveBeenCalledWith("/createQuestion");
    });

    test("should create a /updateQuestion/:id endpoint", () => {
      questionsRouter(app);
      expect(mockPut).toHaveBeenCalledWith("/updateQuestion/:id");
    });

    test("should create a /deleteQuestion/:id endpoint", () => {
      questionsRouter(app);
      expect(mockDelete).toHaveBeenCalledWith("/deleteQuestion/:id");
    });
  });

  describe("successful questionRequests", () => {
    test("should call response.jsonp with the created question when createQuestion completes successfully", async () => {
      questionsRouter(app);
      await flushPromises();
      expect(mockResponse.jsonp).toHaveBeenCalledWith(mockQuestion);
    });
  });

  describe("failing questionRequests", () => {
    test("should call mockNext with an error when createQuestion fails", async () => {
      const error = new Error("rejected");
      questionsRequests.createQuestion.mockRejectedValue(error);

      questionsRouter(app);

      await flushPromises();

      expect(mockNext).toHaveBeenCalledWith(error);
    });

    test("should call mockNext with an error when getQuestions fails", async () => {
      const error = new Error("rejected");
      questionsRequests.getQuestions.mockRejectedValue(error);

      questionsRouter(app);

      await flushPromises();

      expect(mockNext).toHaveBeenCalledWith(error);
    });

    test("should call mockNext with an error when getQuestionById fails", async () => {
      const error = new Error("rejected");
      questionsRequests.getQuestionById.mockRejectedValue(error);

      questionsRouter(app);

      await flushPromises();

      expect(mockNext).toHaveBeenCalledWith(error);
    });

    test("should call mockNext with an error when updateQuestion fails", async () => {
      const error = new Error("rejected");
      questionsRequests.updateQuestion.mockRejectedValue(error);

      questionsRouter(app);

      await flushPromises();

      expect(mockNext).toHaveBeenCalledWith(error);
    });

    test("should call mockNext with an error when deleteQuestion fails", async () => {
      const error = new Error("rejected");
      questionsRequests.deleteQuestion.mockRejectedValue(error);

      questionsRouter(app);

      await flushPromises();

      expect(mockNext).toHaveBeenCalledWith(error);
    });
  });
});
