const userRouter = require("./userRouter");
const userRequests = require("../../requests/users/userRequests");

const mockGet = jest.fn();
const mockPost = jest.fn();

const mockRegister = {};
const mockLogin = {
  success: true,
  token: "Bearer 1",
};

const mockRequest = {
  body: mockRegister,
};

const mockResponse = {
  jsonp: jest.fn(),
  status: jest.fn(),
  send: jest.fn(),
};

const mockNext = jest.fn();

/**
 * We dont care what the output of these are for unit tests, so we can set them as jest mock functions.
 * We can test if they're called at the correct times.
 */
const app = {
  logger: {
    error: jest.fn(),
    info: jest.fn(),
    trace: jest.fn(),
  },
};

/**
 * Mocking express because we are testing the code, not express
 */
jest.mock("express", () => ({
  Router: () => ({
    get: (string, callback) => {
      mockGet(string);
      callback(mockRequest, mockResponse, mockNext);
    },
    post: (string, callback) => {
      mockPost(string);
      callback(mockRequest, mockResponse, mockNext);
    },
  }),
}));

/**
 * Mocking these because I have tested these in isolation in userRequests.spec.js
 * Can mock what I want back from these functions so the test knows how to act
 */
jest.mock("../../requests/users/userRequests.js", () => ({
  registerUser: jest
    .fn()
    .mockImplementation(() => Promise.resolve(mockRegister)),
  loginUser: jest.fn().mockImplementation(() => Promise.resolve(mockLogin)),
}));

const flushPromises = () => new Promise(setImmediate);

describe("userRouter", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe("userRouter initial endpoints", () => {
    test("should create a /userHealth endpoint", () => {
      userRouter(app);
      expect(mockGet).toHaveBeenCalledWith("/userHealth");
      expect(mockResponse.send).toHaveBeenCalledWith(
        "This is a user endpoint health check"
      );
    });

    test("should create a /register endpoint", () => {
      userRouter(app);
      expect(mockPost).toHaveBeenCalledWith("/register");
    });

    test("should create a /login endpoint", () => {
      userRouter(app);
      expect(mockPost).toHaveBeenCalledWith("/login");
    });
  });

  describe("successful questionRequests", () => {
    test("should call response.jsonp with the created question when createQuestion completes successfully", async () => {
      userRouter(app);
      await flushPromises();
      expect(mockResponse.jsonp).toHaveBeenCalledWith(mockRegister);
    });
  });

  describe("failing userRequests", () => {
    test("should call mockNext with an error when registerUser fails", async () => {
      const error = new Error("rejected");
      userRequests.registerUser.mockRejectedValue(error);

      userRouter(app);

      await flushPromises();

      expect(mockNext).toHaveBeenCalledWith(error);
    });

    test("should call mockNext with an error when loginUser fails", async () => {
      const error = new Error("rejected");
      userRequests.loginUser.mockRejectedValue(error);

      userRouter(app);

      await flushPromises();

      expect(mockNext).toHaveBeenCalledWith(error);
    });
  });
});
