const validateRegisterInput = require("./register");

const allFieldsEmpty = {
  name: "",
  email: "",
  password: "",
  password2: "",
};

const emailFieldInvalid = {
  name: "test-name",
  email: "test-email",
  password: "test-password",
  password2: "test-password",
};

const nonMatchingPassword = {
  name: "test-name",
  email: "test@test.com",
  password: "test-password",
  password2: "should not match",
};
describe("validateLoginInput", () => {
  test("should return as not valid if all fields are not filled in", () => {
    expect(validateRegisterInput(allFieldsEmpty)).toEqual({
      errors: {
        name: "Name field is required",
        email: "Email field is required",
        password: "Password must be at least 6 characters",
        password2: "Confirm password field is required",
      },
      isValid: false,
    });
  });

  test("should return as not valid if email is in the wrong format", () => {
    expect(validateRegisterInput(emailFieldInvalid)).toEqual({
      errors: {
        email: "Email is invalid",
      },
      isValid: false,
    });
  });

  test("should return as not valid if password dont match", () => {
    expect(validateRegisterInput(nonMatchingPassword)).toEqual({
      errors: {
        password2: "Passwords must match",
      },
      isValid: false,
    });
  });
});
