const validateLoginInput = require("./login");

const allFieldsEmpty = {
  email: "",
  password: "",
};

const emailFieldEmpty = {
  email: "",
  password: "test-password",
};

const passwordFieldEmpty = {
  email: "test@test.com",
  password: "",
};

const invalidEmail = {
  email: "test-email",
  password: "test-password",
};

const validFields = {
  email: "test@test.com",
  password: "test-password",
};
describe("validateLoginInput", () => {
  test("should return as not valid if both fields are not filled in", () => {
    expect(validateLoginInput(allFieldsEmpty)).toEqual({
      errors: {
        email: "Email field is required",
        password: "Password field is required",
      },
      isValid: false,
    });
  });

  test("should return as not valid if email field is empty", () => {
    expect(validateLoginInput(emailFieldEmpty)).toEqual({
      errors: {
        email: "Email field is required",
      },
      isValid: false,
    });
  });

  test("should return as not valid if password fields is empty", () => {
    expect(validateLoginInput(passwordFieldEmpty)).toEqual({
      errors: {
        password: "Password field is required",
      },
      isValid: false,
    });
  });

  test("should return as not valid if email field is not the right format", () => {
    expect(validateLoginInput(invalidEmail)).toEqual({
      errors: {
        email: "Email is invalid",
      },
      isValid: false,
    });
  });

  test("should return as valid if email field and password field are correct", () => {
    expect(validateLoginInput(validFields)).toEqual({
      errors: {},
      isValid: true,
    });
  });
});
